//
//  MovieDetailUseCaseTest.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import XCTest
import Domain

class MovieDetailUseCaseTests: XCTestCase {

    var useCase: MovieDetailUseCaseProtocol?
    var movieId: Int = 1

    override func setUp() {
        useCase = TestDomainDIContainer.getMovieDetailUseCaseMock()

        movieId = 1
    }

    func testMovieDetailUseCase() {
        let expectation = XCTestExpectation(description: "MovieDetailUseCase")

        useCase?.getMovie(id: movieId) { result in
            switch result {
                case .success(let movie):
                    XCTAssertEqual(movie.id, self.movieId)
                    expectation.fulfill()
                case .failure(let error):
                    XCTFail("Expected to be a success but got a failure with \(error)")
            }
        }

        wait(for: [expectation], timeout: 10.0)
    }

}
