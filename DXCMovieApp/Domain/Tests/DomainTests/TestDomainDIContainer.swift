//
//  TestDomainDIContainer.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Data
import Domain

struct TestDomainDIContainer {

    // MovieRepositoryProtocol Mock
    static func getMovieRepositoryMock() -> MovieRepositoryProtocol {
        let mock = MovieRepositoryMock()

        return mock
    }

    // MovieDetailUseCaseProtocol Mock
    static func getMovieDetailUseCaseMock() -> MovieDetailUseCaseProtocol {
        let mock = MovieDetailUseCaseMock(
            movieRepository: getMovieRepositoryMock()
        )

        let movie = Movie(
            id: 1,
            title: "Test",
            overview: "Test1 overview",
            poster: nil,
            background: nil,
            voteAverage: 2,
            genres: nil,
            duration: nil,
            releaseDate: nil
        )

        mock.executeHandler = { id, completion in
            completion(.success(movie))
        }

        return mock
    }

    // MoviesSearchUseCaseProtocol Mock
    static func getMoviesSearchUseCaseMock() -> MoviesSearchUseCaseProtocol {
        let mock = MoviesSearchUseCaseMock(
            movieRepository: getMovieRepositoryMock()
        )

        let movies = [
            Movie(
                id: 1,
                title: "Test",
                overview: "Test1 overview",
                poster: nil,
                background: nil,
                voteAverage: 2,
                genres: nil,
                duration: nil,
                releaseDate: nil
            )
        ]

        mock.executeHandler = { query, page, completion in
            let result = movies.filter { $0.title.contains(query) }

            completion(
                .success(
                    MovieList(
                        movies: result,
                        totalPages: result.isEmpty ? 0 : 1
                    )
                )
            )
        }

        return mock
    }

}
