//
//  MoviesSearchUseCaseTests.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import XCTest
import Domain

class MoviesSearchUseCaseTests: XCTestCase {

    private var useCase: MoviesSearchUseCaseProtocol?
    private var title: String = ""
    private var page: Int?

    override func setUp() {

        useCase = TestDomainDIContainer.getMoviesSearchUseCaseMock()

        title = "Test"
        page = 1
    }

    func testMoviesUseCase() {
        let expectation = XCTestExpectation(description: "MoviesSearchUseCase")

        useCase?.searchMovies(query: self.title, page: page) { result in
            switch result {
                case .success(let responseList):
                    XCTAssertFalse(responseList.movies.isEmpty)
                    expectation.fulfill()
                case .failure(let error):
                    XCTFail("Expected to be a success but got a failure with \(error)")
            }
        }

        wait(for: [expectation], timeout: 10.0)
    }

}
