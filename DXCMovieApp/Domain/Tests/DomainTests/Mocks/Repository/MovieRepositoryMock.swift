//
//  MovieRepositoryMock.swift
//
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Domain

class MovieRepositoryMock: MovieRepositoryProtocol {

    var searchExecuteHandler: ((String, Int?, @escaping (Result<MovieList, Error>) -> Void) -> Void)?

    var detailExecuteHandler: ((Int, @escaping (Result<Movie, Error>) -> Void) -> Void)?

    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void) {
        if let searchExecuteHandler = searchExecuteHandler {
            searchExecuteHandler(query, page, completion)
        }
    }

    func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void) {
        if let detailExecuteHandler = detailExecuteHandler {
            detailExecuteHandler(id, completion)
        }
    }

}
