//
//  MovieDetailUseCaseMock.swift
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Domain

class MovieDetailUseCaseMock: MovieDetailUseCaseProtocol {

    var movieRepository: MovieRepositoryProtocol

    var executeHandler: ((Int, @escaping (Result<Movie, Error>) -> Void) -> Void)?

    init(movieRepository: MovieRepositoryProtocol) {
        self.movieRepository = movieRepository
    }

    func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void) {
        if let executeHandler = executeHandler {
            executeHandler(id, completion)
        }
    }

}
