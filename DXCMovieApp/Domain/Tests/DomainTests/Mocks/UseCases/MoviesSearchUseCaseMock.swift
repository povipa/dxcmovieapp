//
//  MoviesSearchUseCaseMock.swift
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Domain

class MoviesSearchUseCaseMock: MoviesSearchUseCaseProtocol {

    var movieRepository: MovieRepositoryProtocol

    var executeHandler: ((String, Int?, @escaping (Result<MovieList, Error>) -> Void) -> Void)?

    init(movieRepository: MovieRepositoryProtocol) {
        self.movieRepository = movieRepository
    }

    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void) {
        if let executeHandler = executeHandler {
            executeHandler(query, page, completion)
        }
    }

}
