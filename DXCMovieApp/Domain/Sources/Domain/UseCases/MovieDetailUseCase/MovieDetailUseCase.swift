//
//  MovieDetailUseCase.swift
//  
//
//  Created by Pablo Potel Villar on 22/12/21.
//

public protocol MovieDetailUseCaseProtocol {

    var movieRepository: MovieRepositoryProtocol { get }

    /**
     Get movie details
        - Parameter id: The movie id
        - Returns: Movie if exists
     */
    func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void)

}

public class MovieDetailUseCase: MovieDetailUseCaseProtocol {

    public var movieRepository: MovieRepositoryProtocol

    public init(movieRepository: MovieRepositoryProtocol) {
        self.movieRepository = movieRepository
    }

    public func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void) {
        movieRepository.getMovie(id: id, completion: completion)
    }

}
