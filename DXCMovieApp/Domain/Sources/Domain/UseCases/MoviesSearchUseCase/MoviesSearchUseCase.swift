//
//  MoviesSearchUseCaseProtocol.swift
//  
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import Foundation

public protocol MoviesSearchUseCaseProtocol {

    var movieRepository: MovieRepositoryProtocol { get }

    /**
     Search movies by query
        - Parameters:
            - query: Text to search
            - page: Page
        - Returns: Movies and number of pages associated with searching
     */
    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void)

}

public class MoviesSearchUseCase: MoviesSearchUseCaseProtocol {

    public var movieRepository: MovieRepositoryProtocol

    public init(movieRepository: MovieRepositoryProtocol) {
        self.movieRepository = movieRepository
    }

    public func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void) {
        movieRepository.searchMovies(query: query, page: page, completion: completion)
    }

}
