//
//  Movie.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation

public struct Movie {

    public var id: Int
    public var title: String
    public var overview: String
    public var posterImage: String?
    public var backgroundImage: String?
    public var voteAverage: Double
    public var genres: [Genre]?
    public var duration: Int?
    public var releaseDate: Date?

    public init(
        id: Int,
        title: String,
        overview: String,
        poster: String?,
        background: String?,
        voteAverage: Double,
        genres: [Genre]?,
        duration: Int?,
        releaseDate: Date?
    ) {
        self.id = id
        self.title = title
        self.overview = overview
        self.posterImage = poster
        self.backgroundImage = background
        self.voteAverage = voteAverage
        self.genres = genres
        self.duration = duration
        self.releaseDate = releaseDate
    }

}
