//
//  MovieList.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Foundation

public struct MovieList {

    public var movies: [Movie]
    public var totalPages: Int

    public init(movies: [Movie], totalPages: Int) {
        self.movies = movies
        self.totalPages = totalPages
    }

}
