//
//  Genre.swift
//  
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import Foundation

public struct Genre {

    public var id: Int
    public var name: String

    public init(id: Int, name: String) {
        self.id = id
        self.name = name
    }

}
