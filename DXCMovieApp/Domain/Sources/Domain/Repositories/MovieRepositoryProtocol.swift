//
//  MovieRepositoryProtocol.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation

public protocol MovieRepositoryProtocol {

    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void)

    func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void)

}
