//
//  StoryboardInstatiable.swift
//
//  Created by Pablo Potel Villar on 21/12/2021.
//

import UIKit

public protocol StoryboardInstatiable {
    associatedtype StoryboardType

    static var defaultFileName: String { get }

    static func instantiateViewController(in storyboardName: String?, bundle: Bundle?) -> StoryboardType
}

extension StoryboardInstatiable where Self: UIViewController {

    public static var defaultFileName: String {
        return NSStringFromClass(Self.self).components(separatedBy: ".").last!
    }

    public static func instantiateViewController(in storyboardName: String? = nil, bundle: Bundle? = nil) -> Self {
        let storyboard = UIStoryboard(name: storyboardName ?? self.defaultFileName, bundle: bundle)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: self.defaultFileName) as? Self else {
            fatalError("Cannot instatiate initial view controller \(Self.self) from storyboard with name \(String(describing: storyboardName))")
        }
        return viewController
    }

}
