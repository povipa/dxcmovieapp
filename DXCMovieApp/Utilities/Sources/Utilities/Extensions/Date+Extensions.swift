//
//  Date+Extensions.swift
//  
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import Foundation

extension Date {

    /// Init with dateString and format, default "yyyy-MM-dd"
    public init(dateString: String, format: String = "yyyy-MM-dd") {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = format
        dateStringFormatter.timeZone = TimeZone(abbreviation: "GMT")

        if let date = dateStringFormatter.date(from: dateString) {
            self.init(timeInterval: 0, since: date)
        } else {
            self.init()
        }
    }

    /// Returns "20 July 2020"
    public func printLongFormat() -> String {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "dd MMMM YYYY"
        return dateStringFormatter.string(from: self)
    }

}
