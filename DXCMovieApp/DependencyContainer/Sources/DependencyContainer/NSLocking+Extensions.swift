import Foundation

extension NSLocking {

    @discardableResult
    public func sync<T>(action: () -> T) -> T {
        lock()

        defer { self.unlock() }

        return action()
    }

}
