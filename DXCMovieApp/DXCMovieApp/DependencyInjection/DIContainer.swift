//
//  DIContainer.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import DependencyContainer
import Domain
import Data

protocol Assembler {

    static func assemble(container: DependencyContainer)

}

class DIContainer {

    static let shared = DIContainer()

    let container = DependencyContainer()

    func setup() {
        setupRepositoriesDependencies()
        setupUseCasesDependencies()

        MoviesViewAssembler.assemble(container: container)
        MovieDetailViewAssembler.assemble(container: container)
    }

    private func setupRepositoriesDependencies() {
        container.register(APIServiceProtocol.self) { _ in
            return APIService()
        }

        MovieRepositoryAssembler.assemble(container: container)
    }

    private func setupUseCasesDependencies() {
        container.register(MovieDetailUseCaseProtocol.self) { di in
            return MovieDetailUseCase(
                movieRepository: di.resolve(MovieRepositoryProtocol.self)
            )
        }

        container.register(MoviesSearchUseCaseProtocol.self) { di in
            return MoviesSearchUseCase(
                movieRepository: di.resolve(MovieRepositoryProtocol.self)
            )
        }
    }

}
