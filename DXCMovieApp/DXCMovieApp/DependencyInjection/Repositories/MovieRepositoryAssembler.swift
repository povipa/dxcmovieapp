//
//  MovieRepositoryAssembler.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import DependencyContainer
import Data
import Domain

class MovieRepositoryAssembler: Assembler {

    static func assemble(container: DependencyContainer) {
        container.register(MovieLocalDataSourceProtocol.self) { _ in
            return MovieLocalDataSource()
        }

        container.register(MovieRemoteDataSourceProtocol.self) { di in
            return MovieRemoteDataSource(apiService: di.resolve(APIServiceProtocol.self))
        }

        container.register(MovieRepositoryProtocol.self) { di in
            return MovieRepository(
                localDataSource: di.resolve(MovieLocalDataSourceProtocol.self),
                remoteDataSource: di.resolve(MovieRemoteDataSourceProtocol.self)
            )
        }
    }

}
