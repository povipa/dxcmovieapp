//
//  MoviesViewAssembler.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import DependencyContainer
import Domain

class MoviesViewAssembler: Assembler {

    static func assemble(container: DependencyContainer) {
        container.register(MoviesViewModelProtocol.self) { di in
            return MoviesViewModel(
                moviesUseCase: di.resolve(MoviesSearchUseCaseProtocol.self)
            )
        }
    }

}
