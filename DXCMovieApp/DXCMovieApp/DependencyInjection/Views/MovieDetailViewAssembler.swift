//
//  MovieDetailViewAssembler.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import DependencyContainer
import Domain

class MovieDetailViewAssembler: Assembler {

    static func assemble(container: DependencyContainer) {
        container.register(MovieDetailViewModelProtocol.self) { (di, id: Int) in
            return MovieDetailViewModel(
                id: id,
                movieDetailUseCase: di.resolve(MovieDetailUseCaseProtocol.self)
            )
        }
    }

}
