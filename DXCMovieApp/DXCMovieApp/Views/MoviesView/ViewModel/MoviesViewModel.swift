//
//  MoviesViewModel.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation
import Domain

protocol MoviesViewModelProtocol {

    var minCharacters: Int { get }
    var didUpdate: () -> Void { get set }
    var movies: [MovieListVM] { get }

    func getMovies(query: String)
    func nextMovies()

}

class MoviesViewModel: MoviesViewModelProtocol {

    private let moviesUseCase: MoviesSearchUseCaseProtocol
    private var currentPage: Int = 1
    private var totalPages: Int = 0
    private var currentQuery: String = ""

    var minCharacters: Int = 3
    var didUpdate: () -> Void = { () -> Void in }
    var movies: [MovieListVM] = [] {
        didSet {
            didUpdate()
        }
    }

    init(moviesUseCase: MoviesSearchUseCaseProtocol) {
        self.moviesUseCase = moviesUseCase
    }

    func getMovies(query: String) {
        currentQuery = query
        currentPage = 1
        findMovies(query: query, page: currentPage, isNew: true)
    }

    func nextMovies() {
        if currentPage < totalPages {
            currentPage += 1
            findMovies(query: currentQuery, page: currentPage, isNew: false)
        }
    }

    private func findMovies(query: String, page: Int, isNew: Bool) {
        if query.isEmpty == false && query.count >= minCharacters {
            if isNew {
                movies = [] // Clear movies when new searching
            }

            moviesUseCase.searchMovies(query: query, page: page) { result in
                switch result {
                    case .success(let moviesList):
                        self.totalPages = moviesList.totalPages
                        self.movies.append(
                            contentsOf: moviesList.movies.map {
                                MovieListVM.toModel(movie: $0)
                            }
                       )
                    case .failure(let error):
                        print("MoviesViewModel ERROR => \(error.localizedDescription)")
                }
            }
        }
    }

}

struct MovieListVM {
    var id: Int
    var image: String
    var title: String
    var releaseDate: String
    var overview: String
    var voteAverage: String
    var votePercentage: Double

    static func toModel(movie: Movie) -> MovieListVM {
        return MovieListVM(
            id: movie.id,
            image: movie.posterImage ?? "",
            title: movie.title,
            releaseDate: movie.releaseDate?.printLongFormat() ?? "",
            overview: movie.overview,
            voteAverage: String(format: "%.01f", movie.voteAverage),
            votePercentage: movie.voteAverage * 10
        )
    }
}
