//
//  MoviesCoordinator.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit

class MoviesCoordinator: Coordinator {

    enum Route: Router {
        case movieDetail(Int)
    }

    var navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start() {
        let controller = MoviesViewController.create(
            coordinator: self,
            viewModel: DIContainer.shared.container.resolve(MoviesViewModelProtocol.self)
        )

        navigationController?.pushViewController(controller, animated: true)
    }

    func navigateTo(route: Router) {
        guard let router = route as? MoviesCoordinator.Route else {
            fatalError("navigateTo needs MoviesCoordinator")
        }

        switch router {
            case .movieDetail(let id):
                goToMovieDetail(id: id)
        }
    }

}

// MARK: - Navigate
extension MoviesCoordinator {

    private func goToMovieDetail(id: Int) {
        let coordinator = MovieDetailCoordinator(navigationController: navigationController!)
        coordinator.start(id: id)
    }

}
