//
//  MoviesViewController.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit
import Utilities

class MoviesViewController: UITableViewController, StoryboardInstatiable {

    private let searchController = UISearchController(searchResultsController: nil)

    private let activityIndicator = UIActivityIndicatorView()
    private let loadingView = UIView()

    private let cellIdentifier = "movieCell"
    private var isFetchingMovies = false

    private var coordinator: Coordinator?
    private var viewModel: MoviesViewModelProtocol!

    final class func create(coordinator: Coordinator, viewModel: MoviesViewModelProtocol!) -> MoviesViewController {
        let view = MoviesViewController.instantiateViewController(in: "MoviesView", bundle: .main)
        view.coordinator = coordinator
        view.viewModel = viewModel

        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        bind()
        initView()
    }

    private func initView() {
        title = "DXCMovies"

        tableView.register(
            UINib(nibName: "MoviesTableViewCell", bundle: .main),
            forCellReuseIdentifier: self.cellIdentifier
        )
        tableView.tableFooterView = UIView()
        tableView.prefetchDataSource = self

        configLoader()
        configSearchBar()
    }

    private func configLoader() {
        let size: CGSize = CGSize(width: 60, height: 60)
        let posX = (tableView.frame.width / 2) - (size.width / 2)
        let posY = (tableView.frame.height / 2) - (size.height / 2) - (navigationController?.navigationBar.frame.height ?? 0)
        loadingView.frame = CGRect(x: posX, y: posY, width: size.width, height: size.height)

        activityIndicator.style = .large
        activityIndicator.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        activityIndicator.hidesWhenStopped = true

        loadingView.addSubview(activityIndicator)
        tableView.addSubview(loadingView)
    }

    private func configSearchBar() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.hidesSearchBarWhenScrolling = false

        UISearchBar.appearance().tintColor = .white

        searchController.searchBar.searchTextField.placeholder = "Search movies..."
        searchController.searchBar.searchTextField.tintColor = .black
        searchController.searchBar.searchTextField.textColor = .black
        searchController.searchBar.searchTextField.backgroundColor = .white

        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [.foregroundColor: UIColor.black]

        navigationItem.searchController = searchController
    }

    private func showLoading() {
        activityIndicator.startAnimating()
    }

    private func hideLoading() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.activityIndicator.stopAnimating()
        }
    }

}

// MARK: - Functions related to the viewModel
extension MoviesViewController {

    private func bind() {
        viewModel.didUpdate = { [weak self] () in
            DispatchQueue.main.async {
                self?.isFetchingMovies = false
                self?.hideLoading()
                self?.reloadData()
            }
        }
    }

    private func search(query: String) {
        showLoading()
        viewModel.getMovies(query: query)
    }

    private func getNext() {
        isFetchingMovies = true
        viewModel.nextMovies()
    }

    private func reloadData() {
        tableView.reloadData()
    }

}

// MARK: - Function to prefeching movies
extension MoviesViewController: UITableViewDataSourcePrefetching {

    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if indexPath.row >= viewModel.movies.count - 3 && isFetchingMovies == false {
                getNext()
                break
            }
        }
    }

}

// MARK: - Functions reated to TableView delegate and dataSource
extension MoviesViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.movies.isEmpty {
            if activityIndicator.isAnimating == false {
                tableView.setEmptyMessage(
                    message: "Type at least 3 characters in the searchBar to search movies",
                    color: .primaryColor
                )
            }
        } else {
            tableView.restore()
        }

        return viewModel.movies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? MoviesTableViewCell {
            let movie = viewModel.movies[indexPath.row]

            cell.fillData(movie: movie)

            return cell
        }

        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = viewModel.movies[indexPath.row]
        coordinator?.navigateTo(route: MoviesCoordinator.Route.movieDetail(movie.id))
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }

}

// MARK: - Functions to Search
extension MoviesViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= viewModel.minCharacters {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(performSearch), object: nil)
            self.perform(#selector(performSearch), with: nil, afterDelay: 2.0)
        }
    }

    @objc private func performSearch() {
        if let query = searchController.searchBar.text, query.isEmpty == false {
            search(query: query)
        }
    }

}
