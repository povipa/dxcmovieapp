//
//  MoviesTableViewCell.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {

    @IBOutlet private weak var movieImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var voteAverageView: VoteAverageCircle!

    override func awakeFromNib() {
        super.awakeFromNib()

        movieImageView.layer.cornerRadius = 8
        movieImageView.layer.masksToBounds = true

        voteAverageView.lineColor = .primaryColor
        voteAverageView.textColor = .black
        voteAverageView.lineWidth = 8.0
    }

    func fillData(movie: MovieListVM) {
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        dateLabel.text = movie.releaseDate
        movieImageView.setKFImage(url: URL(string: movie.image))

        voteAverageView.isHidden = movie.votePercentage <= 0
        if movie.votePercentage > 0 {
            voteAverageView.percent = CGFloat(movie.votePercentage)
        }
    }

}
