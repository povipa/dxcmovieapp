//
//  VoteAverageCircle.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import Foundation
import UIKit

class VoteAverageCircle: UIView {

    var percent: Double = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    var lineColor: UIColor = .blue
    var lineWidth: CGFloat = 3.0
    var textColor: UIColor = .black

    private let shapeLayer = CAShapeLayer()
    private let label = CATextLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func draw(_ rect: CGRect) {
        if percent > 0 {
            layer.sublayers?.forEach { $0.removeFromSuperlayer() }

            percent = min(100, percent)
            let startAngle = -CGFloat(Double.pi / 2)
            let endAngle = ((CGFloat(Double.pi * 2) * percent) / 100) + startAngle

            drawCircle(
                lineWidth: lineWidth,
                lineColor: lineColor,
                startAngle: startAngle,
                endAngle: endAngle
            )

            drawText(value: percent / 10)
        }
    }

    // Draw percentage in the middle of the circle
    func drawText(value: CGFloat) {
        let size = ("\(value)" as NSString)
            .size(withAttributes: [.font: UIFont(name: "Avenir", size: 13)])

        label.fontSize = 13
        label.frame = CGRect(
            x: bounds.width / 2 - size.width / 2,
            y: bounds.height / 2 - size.height / 2,
            width: 30, height: 20
        )
        label.string = "\(value)"
        label.foregroundColor = textColor.cgColor

        layer.addSublayer(label)
    }

    // Draw circle
    func drawCircle(lineWidth: CGFloat, lineColor: UIColor, startAngle: CGFloat, endAngle: CGFloat) {
        let size: CGFloat = bounds.size.width / 2

        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x: size, y: size),
            radius: CGFloat(size - (lineWidth / 2)),
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true
        )

        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineCap = .round

        layer.addSublayer(shapeLayer)
    }

}
