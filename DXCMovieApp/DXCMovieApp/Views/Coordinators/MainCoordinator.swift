//
//  MainCoordinator.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit

class MainCoordinator: Coordinator {

    var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.setStatusBar(style: .default)
    }

    func start() {
        let coordinator = MoviesCoordinator(navigationController: navigationController)
        coordinator.start()
    }

    func navigateTo(route: Router) { }

}
