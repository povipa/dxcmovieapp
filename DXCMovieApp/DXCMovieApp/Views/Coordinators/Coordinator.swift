//
//  Coordinator.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit

protocol Router { }

protocol Coordinator: AnyObject {

    var navigationController: UINavigationController? { get set }

    func navigateTo(route: Router)

}

extension Coordinator {

    func setStatusBar(style: UIBarStyle = .default) {
        self.navigationController?.navigationBar.barStyle = style
    }

}
