//
//  MovieDetailViewController.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit
import Utilities

class MovieDetailViewController: BaseViewController, StoryboardInstatiable {

    @IBOutlet private weak var contentScrollView: UIScrollView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var durationLabel: UILabel!
    @IBOutlet private weak var genresLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var voteAverageView: VoteAverageCircle!

    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!

    private var coordinator: Coordinator?
    private var viewModel: MovieDetailViewModelProtocol!

    final class func create(coordinator: Coordinator, viewModel: MovieDetailViewModelProtocol) -> MovieDetailViewController {
        let view = MovieDetailViewController.instantiateViewController(in: "MovieDetailView", bundle: .main)
        view.coordinator = coordinator
        view.viewModel = viewModel

        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
        initView()

        getMovie()
    }

    private func initView() {
        title = "DXCMovies"
        configVoteCircleView()

        activityIndicator.hidesWhenStopped = true

        contentScrollView.alpha = 0
        showLoading()
    }

    private func configVoteCircleView() {
        voteAverageView.lineColor = .primaryColor
        voteAverageView.textColor = .white
        voteAverageView.lineWidth = 8.0
    }

    func showLoading() {
        activityIndicator.startAnimating()
    }

    func hideLoading() {
        activityIndicator.stopAnimating()
    }

}

// MARK: - Functions related to the viewModel
extension MovieDetailViewController {

    private func bind() {
        viewModel.didUpdate = { [weak self] () in
            DispatchQueue.main.async {
                self?.reloadData()
                UIView.animate(withDuration: 0.25, animations: {
                    self?.contentScrollView.alpha = 1
                })
                self?.hideLoading()
            }
        }
    }

    private func getMovie() {
        viewModel.getMovie()
    }

    private func reloadData() {
        guard let movie = viewModel.movie else { return }

        backgroundImageView.setKFImage(url: URL(string: movie.image))
        titleLabel.text = movie.title
        durationLabel.text = "Duration: " + movie.duration
        genresLabel.text = movie.genres.map { "• \($0)" }.joined(separator: " ")
        overviewLabel.text = movie.overview.isEmpty ? "No overview available" : movie.overview

        voteAverageView.percent = CGFloat(movie.votePercentage)
    }

}
