//
//  MovieDetailCoordinator.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import UIKit

class MovieDetailCoordinator: Coordinator {

    enum Route: Router { }

    var navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start(id: Int) {
        let controller = MovieDetailViewController.create(
            coordinator: self,
            viewModel: DIContainer.shared.container.resolve(MovieDetailViewModelProtocol.self, argument: id)
        )

        navigationController?.pushViewController(controller, animated: true)
    }

    func navigateTo(route: Router) { }

}
