//
//  MovieViewModel.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation
import Domain

protocol MovieDetailViewModelProtocol {

    var didUpdate: () -> Void { get set }
    var movie: MovieDetailMV? { get }

    func getMovie()
}

class MovieDetailViewModel: MovieDetailViewModelProtocol {

    private let movieDetailUseCase: MovieDetailUseCaseProtocol
    private let id: Int

    var didUpdate: () -> Void = { () -> Void in }
    var movie: MovieDetailMV? {
        didSet {
            didUpdate()
        }
    }

    init(id: Int, movieDetailUseCase: MovieDetailUseCaseProtocol) {
        self.id = id
        self.movieDetailUseCase = movieDetailUseCase
    }

    func getMovie() {
        movieDetailUseCase.getMovie(id: id) { result in
            switch result {
                case .success(let domainMovie):
                    self.movie = MovieDetailMV.toModel(movie: domainMovie)
                case .failure(let error):
                    print("MovieDetailViewModel ERROR => \(error.localizedDescription)")
            }
        }
    }

}

struct MovieDetailMV {
    var image: String
    var title: String
    var duration: String
    var genres: [String]
    var overview: String
    var voteAverage: String
    var votePercentage: Double

    /// Format duration to HH:mm
    private static func formatDuration(duration: Double) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .positional
        if let value = formatter.string(from: duration) {
            return value + "h"
        }
        return "N/A"
    }

    static func toModel(movie: Movie) -> MovieDetailMV {
        return MovieDetailMV(
            image: movie.backgroundImage ?? movie.posterImage ?? "",
            title: movie.title,
            duration: self.formatDuration(duration: Double(movie.duration ?? 0)),
            genres: movie.genres?.map { $0.name } ?? [],
            overview: movie.overview,
            voteAverage: String(format: "%.01f", movie.voteAverage),
            votePercentage: movie.voteAverage * 10
        )
    }
}
