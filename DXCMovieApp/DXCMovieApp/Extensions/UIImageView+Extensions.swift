//
//  UIImageView+Extensions.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import UIKit
import Kingfisher

extension UIImageView {

    func setKFImage(url: URL?, placeholder: Placeholder? = UIImage(named: "placeholder")) {
        self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.1))], completionHandler: nil)
    }

}
