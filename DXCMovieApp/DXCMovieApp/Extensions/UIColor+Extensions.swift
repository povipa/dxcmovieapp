//
//  UIColor+Extensions.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import UIKit

extension UIColor {

    static let primaryColor = UIColor(named: "primaryColor")!

}
