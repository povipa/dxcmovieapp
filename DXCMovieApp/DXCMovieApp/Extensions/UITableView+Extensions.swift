//
//  UITableView+Extensions.swift
//  DXCMovieApp
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import UIKit

extension UITableView {

    // Show empty message
    func setEmptyMessage(message: String, color: UIColor) {
        let messageLabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 0,
                width: bounds.size.width,
                height: bounds.size.height
            )
        )
        messageLabel.text = message
        messageLabel.textColor = color
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        if let descriptor = UIFontDescriptor(name: "Avenir", size: 24).withSymbolicTraits(.traitBold) {
            messageLabel.font = UIFont(descriptor: descriptor, size: 0)
        }
        messageLabel.sizeToFit()

        backgroundView = messageLabel
        separatorStyle = .none
    }

    func restore() {
        backgroundView = nil
        separatorStyle = .singleLine
    }

}
