//
//  TestDataDIContainer.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Data
import Domain

struct TestDataDIContainer {

    // APIServiceProtocol Mock
    static func getAPIServiceMock() -> APIServiceProtocol {
        return APIService()
    }

    // MovieLocalDataSourceProtocol Mock
    static func getMovieLocalDataSourceMock() -> MovieLocalDataSourceProtocol {
        let mock = MovieLocalDataSourceMock()

        return mock
    }

    // MovieRemoteDataSourceProtocol Mock
    static func getMovieRemoteDataSourceMock() -> MovieRemoteDataSourceProtocol {
        let mock = MovieRemoteDataSourceMock(apiService: getAPIServiceMock())

        mock.detailExecuteHandler = { id, completion in
            mock.apiService.run(
                type: MovieResponse.self,
                request: APIConfig.getRequest(for: .getMovie(id: id)),
                completion: completion
            )
        }

        mock.searchExecuteHandler = { query, page, completion in
            mock.apiService.run(
                type: MoviesSearchResponse.self,
                request: APIConfig.getRequest(for: .searchMovies(query: query, page: page)),
                completion: completion
            )
        }

        return mock
    }

    // MovieRepositoryProtocol Mock
    static func getMovieRepositoryMock() -> MovieRepositoryProtocol {
        let mock = MovieRepositoryMock(
            localDataSource: getMovieLocalDataSourceMock(),
            remoteDataSource: getMovieRemoteDataSourceMock()
        )

        mock.detailExecuteHandler = { id, completion in
            mock.remoteDataSource.getMovie(id: id, completion: { result in
                switch result {
                    case .success(let movie):
                        completion(.success(movie.toDomain()))
                    case .failure(let error):
                        completion(.failure(error))
                }
            })
        }

        mock.searchExecuteHandler = { query, page, completion in
            mock.remoteDataSource.searchMovies(query: query, page: page, completion: { result in
                    switch result {
                        case .success(let response):
                            completion(.success(response.toDomain()))
                        case .failure(let error):
                            completion(.failure(error))
                    }
            })
        }

        return mock
    }

}
