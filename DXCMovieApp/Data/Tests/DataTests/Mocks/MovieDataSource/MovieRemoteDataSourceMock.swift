//
//  MovieRemoteDataSourceMock.swift
//  
//
//  Created by Pablo Potel Villar on 23/12/21.
//

import Data
import Domain

class MovieRemoteDataSourceMock: MovieRemoteDataSourceProtocol {

    var searchExecuteHandler: ((String, Int?, @escaping (Result<MoviesSearchResponse, Error>) -> Void) -> Void)?

    var detailExecuteHandler: ((Int, @escaping (Result<MovieResponse, Error>) -> Void) -> Void)?

    var apiService: APIServiceProtocol

    init(apiService: APIServiceProtocol) {
        self.apiService = apiService
    }

    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MoviesSearchResponse, Error>) -> Void) {
        if let searchExecuteHandler = searchExecuteHandler {
            searchExecuteHandler(query, page, completion)
        }
    }

    func getMovie(id: Int, completion: @escaping (Result<MovieResponse, Error>) -> Void) {
        if let detailExecuteHandler = detailExecuteHandler {
            detailExecuteHandler(id, completion)
        }
    }

}
