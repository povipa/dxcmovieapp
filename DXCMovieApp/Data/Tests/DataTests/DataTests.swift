import XCTest
@testable import Data
import Domain

final class DataTests: XCTestCase {

    private var dataSource: MovieRemoteDataSourceProtocol?
    private var movieId: Int = 1
    private var title: String = ""

    override func setUp() {
        dataSource = TestDataDIContainer.getMovieRemoteDataSourceMock()

        movieId = 480912
        title = "Alien: Covenant - Prologue: Phobos"
    }

    func testMovieDetailRemoteDataSource() {
        let expectation = XCTestExpectation(description: "MovieDetailDataSource")

        dataSource?.getMovie(id: movieId, completion: { result in
            switch result {
                case .success(let movie):
                    XCTAssertEqual(movie.toDomain().title, self.title)

                    expectation.fulfill()
                case .failure(let error):
                    XCTFail("Expected to be a success but got a failure with \(error)")

            }
        })

        wait(for: [expectation], timeout: 10.0)
    }

    func testMovieSearchRemoteDataSource() {
        let expectation = XCTestExpectation(description: "MovieSearchDataSource")

        dataSource?.searchMovies(query: title, page: nil, completion: { result in
            switch result {
                case .success(let movieList):
                    XCTAssertEqual(movieList.results.count, 1)

                    expectation.fulfill()
                case .failure(let error):
                    XCTFail("Expected to be a success but got a failure with \(error)")

            }
        })

        wait(for: [expectation], timeout: 10.0)
    }
}
