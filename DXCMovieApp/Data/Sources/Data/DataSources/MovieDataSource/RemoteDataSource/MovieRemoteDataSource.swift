//
//  MovieRemoteDataSource.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

public protocol MovieRemoteDataSourceProtocol {

    var apiService: APIServiceProtocol { get }

    func searchMovies(query: String, page: Int?, completion: @escaping (Result<MoviesSearchResponse, Error>) -> Void)
    func getMovie(id: Int, completion: @escaping (Result<MovieResponse, Error>) -> Void)

}

public final class MovieRemoteDataSource: MovieRemoteDataSourceProtocol {

    public var apiService: APIServiceProtocol

    public init(apiService: APIServiceProtocol) {
        self.apiService = apiService
    }

    public func searchMovies(query: String, page: Int?, completion: @escaping (Result<MoviesSearchResponse, Error>) -> Void) {
        apiService.run(
            type: MoviesSearchResponse.self,
            request: APIConfig.getRequest(for: APIEndPoints.searchMovies(query: query, page: page)),
            completion: completion
        )
    }

    public func getMovie(id: Int, completion: @escaping (Result<MovieResponse, Error>) -> Void) {
        apiService.run(
            type: MovieResponse.self,
            request: APIConfig.getRequest(for: APIEndPoints.getMovie(id: id)),
            completion: completion
        )
    }

}
