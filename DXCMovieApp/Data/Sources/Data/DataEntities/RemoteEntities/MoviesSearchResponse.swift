//
//  MoviesSearchResponse.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation
import Domain

public struct MoviesSearchResponse: Decodable {

    var page: Int
    var results: [MovieResponse]
    var totalResults: Int
    var totalPages: Int

    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }

    public func toDomain() -> MovieList {
        return MovieList(
            movies: results.map { $0.toDomain() },
            totalPages: totalPages
        )
    }

}
