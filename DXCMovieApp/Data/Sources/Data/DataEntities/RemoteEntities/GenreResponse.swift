//
//  GenreResponse.swift
//  
//
//  Created by Pablo Potel Villar on 22/12/21.
//

import Domain

public struct GenreResponse: Decodable {

    var id: Int
    var name: String

    public func toDomain() -> Genre {
        return Genre(
            id: id,
            name: name
        )
    }

}
