//
//  MovieResponse.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Domain
import Utilities
import Foundation

public struct MovieResponse: Decodable {

    var id: Int
    var posterPath: String?
    var backgroundPath: String?
    var overview: String
    var title: String
    var voteAverage: Double
    var genres: [GenreResponse]?
    var runtime: Int?
    var releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case posterPath = "poster_path"
        case backgroundPath = "backdrop_path"
        case overview
        case title
        case voteAverage = "vote_average"
        case genres
        case runtime
        case releaseDate = "release_date"
    }

    public func toDomain() -> Movie {
        let date = releaseDate != nil ? Date(dateString: releaseDate!, format: "yyyy-MM-dd") : nil

        return Movie(
            id: id,
            title: title,
            overview: overview,
            poster: posterPath != nil ? (APIConfig.posterImageURL + posterPath!) : nil,
            background: backgroundPath != nil ? (APIConfig.bigImageURL + backgroundPath!) : nil,
            voteAverage: voteAverage,
            genres: genres?.map { $0.toDomain() },
            duration: runtime,
            releaseDate: date
        )
    }
}
