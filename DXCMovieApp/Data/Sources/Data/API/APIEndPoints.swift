//
//  APIEndPoints.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation

public enum APIEndPoints {
    case searchMovies(query: String, page: Int?)
    case getMovie(id: Int)

    // EndPoint path
    var path: String {
        switch self {
            case .searchMovies(query: _, page: _):
                return "/search/movie/"
            case .getMovie(let id):
                return "/movie/\(id)"
        }
    }

    // EndPoint url params
    var params: String? {
        switch self {
            case .searchMovies(query: let query, page: let page):
                return
                    "&page=\(page ?? 1)" +
                    "&query=\(query.replacingOccurrences(of: " ", with: "%20"))"
            case .getMovie(_):
                return nil
        }
    }
}
