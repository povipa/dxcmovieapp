//
//  APIService.swift
//
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation

public protocol APIServiceProtocol {

    func run<T: Decodable>(type: T.Type, request: URLRequest?, completion: @escaping (Result<T, Error>) -> Void)

}

public class APIService: APIServiceProtocol {

    enum APIError: Error {
        case invalidResponse
        case invalidRequest
        case invalidData
        case invalidDecoding
        case network(Error)
    }

    public init() { }

    public func run<T: Decodable>(type: T.Type, request: URLRequest?, completion: @escaping (Result<T, Error>) -> Void) {
        
        guard let request = request else {
            return completion(.failure(APIService.APIError.invalidRequest))
        }

        print(request.description)

        return URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            if let error = error {
                completion(.failure(APIService.APIError.network(error)))
                return
            }

            guard let response = response as? HTTPURLResponse, 200 ... 299 ~= response.statusCode else {
                completion(.failure(APIService.APIError.invalidResponse))
                return
            }

            guard let data = data else {
                completion(.failure(APIService.APIError.invalidData))
                return
            }

            do {
                completion(.success(try JSONDecoder().decode(T.self, from: data)))
            } catch let error {
                print("APIService => An error has occurred: \(error.localizedDescription))")
                completion(.failure(APIService.APIError.invalidDecoding))
            }
        }).resume()
    }

}
