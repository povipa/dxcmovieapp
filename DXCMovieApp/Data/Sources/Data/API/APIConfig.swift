//
//  APIConfig.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Foundation

public struct APIConfig {

    static var baseURL = "https://api.themoviedb.org/3"
    static var apiKey = "81cfd631f4cd55a3ae1550e21be76396"

    static var bigImageURL = "https://image.tmdb.org/t/p/original/"
    static var posterImageURL = "https://image.tmdb.org/t/p/w200/"

    public static func getRequest(for endpoint: APIEndPoints) -> URLRequest? {
        guard let url = getUrl(endpoint: endpoint) else { return nil }
        return URLRequest(url: url)
    }

    private static func getUrl(endpoint: APIEndPoints) -> URL? {
        return URL(
            string: APIConfig.baseURL
                .appending(endpoint.path)
                .appending("?api_key=\(apiKey)")
                .appending(endpoint.params ?? "")
        )
    }

}
