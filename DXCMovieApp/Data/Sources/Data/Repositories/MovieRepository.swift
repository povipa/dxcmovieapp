//
//  MovieRepository.swift
//  
//
//  Created by Pablo Potel Villar on 21/12/21.
//

import Domain

public class MovieRepository: MovieRepositoryProtocol {

    private let localDataSource: MovieLocalDataSourceProtocol
    private let remoteDataSource: MovieRemoteDataSourceProtocol

    public init(localDataSource: MovieLocalDataSourceProtocol, remoteDataSource: MovieRemoteDataSourceProtocol) {
        self.localDataSource = localDataSource
        self.remoteDataSource = remoteDataSource
    }

    public func searchMovies(query: String, page: Int?, completion: @escaping (Result<MovieList, Error>) -> Void) {
        remoteDataSource.searchMovies(query: query, page: page) { result in
            switch result {
                case .success(let response):
                    completion(.success(response.toDomain()))
                case .failure(let error):
                    completion(.failure(error))
            }
        }
    }

    public func getMovie(id: Int, completion: @escaping (Result<Movie, Error>) -> Void) {
        remoteDataSource.getMovie(id: id, completion: { result in
            switch result {
                case .success(let movie):
                    completion(.success(movie.toDomain()))
                case .failure(let error):
                    completion(.failure(error))
            }
        })
    }

}
