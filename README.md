# DXCMovies

Esta demo ha sido desarrollada usando Swift 5.4.2 y XCode 13.

En este repositorio encontrarás una demo práctica del desarrollo de una aplicación iOS utilizando las APIs públicas de The Movie Database.

## Funcionalidades

1 - Lista de películas: mostrara a través de una llamada a una API una lista de películas, la lista incluye:
	- Cartel de la película
	- Título
	- Descripción completa
	- Media de voto

2 - Detalle de película: Ver los detalles de una película en concreto al hacer clic en el mismo.

## Arquitectura

Para el desarrollo de las funcionalidades listadas en los puntos anteriores, se ha optado por una arquitectura MVVM junto con el patrón Coordinator para manejar la navegación, y a mayores aplicando una capa Clean por encima agrupando las diferentes capas en paquetes con Swift Package Manager.

1 - Paquete Domain: Contiene los casos de uso para el detalle y el listado, así como la definición de los repositorios y los modelos de dominio.

2 - Paquete Data: Contiene la implementación de los repositorios, sus fuentes de datos y entidades, y la definición e implementación del cliente HTTP para realizar las llamadas a la API.

3 - Paquete Utilities: Contiene alguna utilidad a usar en el proyecto.

## Test

Se han hecho test de la capa de Data y de Domain.

## Librerías

Para la inyección de dependencias, se ha utilizado una librería llamada DependencyContainer, que permite marcar las instancias que vamos a aplicar a cada Protocolo de manera sencilla a través de un contenedor de dependencias.

Para el cacheo y manejo de imágenes se ha usado la librería Kingfisher.
